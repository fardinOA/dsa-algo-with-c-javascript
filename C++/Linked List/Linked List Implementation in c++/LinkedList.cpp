#include"bits/stdc++.h"
using namespace std;
typedef long long int ll;
typedef int64_t i64;
#define fast ios_base::sync_with_stdio(false); cin.tie(NULL); cout.tie(NULL);
#define nl "\n"

// define the insert & show function for the linked list
void insert_at_the_begining_of_linked_list(ll x);
void show_all_element_of_linked_list();
void insert_at_the_end_of_linked_list(ll x);

// define a node
struct Node {
  ll data;
  Node* next_node;
};

 Node* headNode;

void insert_at_the_begining_of_linked_list(ll x){
 Node* temp=new Node(); // also i can write as Node* temp=(Node*)malloc(sizeof(struct Node));
 temp->data=x;
 temp->next_node=headNode;
 headNode=temp;

}

void insert_at_the_end_of_linked_list(ll x){
 Node* temp=new Node(); // also i can write as Node* temp=(Node*)malloc(sizeof(struct Node));
 temp->data=x;
 temp->next_node=NULL;

 if(headNode==NULL){
    headNode=temp;
 }else {
    Node* temp2=headNode;
    while(true){
            if(temp2->next_node==NULL)break;
           temp2=temp2->next_node;
    }
     temp2->next_node=temp;
  }


}




void show_all_element_of_linked_list(){
  Node* temp=headNode;
  cout<<"the linked list is:  ";
  while(temp!=NULL){
    cout<<temp->data<<" ";
    temp=temp->next_node;
  }
  cout<<nl;

}
int main()
{
   fast
   headNode=NULL;
   ll t;
   cout<<"How many numbers?"<<endl;
   cin>>t;
   while(t--){
    ll x;
    cout<<"Enter The number"<<endl;
    cin>>x;
    //insert_at_the_begining_of_linked_list(x);
    insert_at_the_end_of_linked_list(x);
    show_all_element_of_linked_list();
   }
}
