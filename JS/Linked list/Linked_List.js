class Node {
    constructor(value) {
        this.data = value;
        this.next_node = null;
    }
}

class singlyLinkedList {
    constructor() {
        this.head = null;
        this.tail = null;
        this.length = 0;
    }

    push(data) {
        let newNode = new Node(data);
        if (!this.head) {
            this.head = newNode;
            this.tail = this.head;
        } else {
            this.tail.next_node = newNode;
            this.tail = newNode;
        }
        this.length++;
        return this;
    }
}

let list = new singlyLinkedList();
list.push(1);
list.push(2);
list.push(3);
list.push(4);
list.push(5);

console.log(list.head);
